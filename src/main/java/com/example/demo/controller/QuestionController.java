package com.example.demo.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.QuestionCreateDto;
import com.example.demo.dto.QuestionDto;
import com.example.demo.dto.QuestionFilterDto;
import com.example.demo.dto.QuestionUpdateDto;
import com.example.demo.dto.ResultDto;
import com.example.demo.exception.ApiException;
import com.example.demo.service.QuestionService;

@RestController
@RequestMapping("/api/question")
class QuestionController {

	private final Logger log = LoggerFactory.getLogger(QuestionController.class);

	@Autowired
	private QuestionService service;

	@GetMapping
	Collection<QuestionDto> questions(QuestionFilterDto filter) {
		return service.find(filter);
	}

	@GetMapping("/{id}")
	ResponseEntity<?> question(@PathVariable Long id) {
		
		Optional<QuestionDto> result = service.find(id);
		return result.map(response -> ResponseEntity.ok().body(response)).orElse(ResponseEntity.notFound().build());
	}

	@PostMapping
	ResponseEntity<ResultDto> create(
			@Valid @RequestBody QuestionCreateDto dto
			) throws URISyntaxException, ApiException {

		log.info("Request to create question: ", dto);
		Long idQuestion = service.create(dto);
		return ResponseEntity.created(new URI("/api/question/" + idQuestion))
				.body(new ResultDto(idQuestion, HttpStatus.CREATED));
	}

	@PutMapping("/{id}")
	ResponseEntity<ResultDto> update(
			@PathVariable Long id, 
			@Valid @RequestBody QuestionUpdateDto dto
			) throws ApiException {
		
		log.info("Request to update question: ", dto);
		service.update(dto, id);
		return ResponseEntity.ok()
				.body(new ResultDto(id, HttpStatus.OK));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) throws ApiException {
		
		log.info("Request to delete question: ", id);
		service.remove(id);
		return ResponseEntity.ok().body("Question removed: " + id);
	}

}