package com.example.demo.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.demo.dto.UserDto;
import com.example.demo.exception.ApiException;
import com.example.demo.exception.ApiExceptionDomain.ExceptionType;

@Component
public class UserComponent {
	
	private final Logger log = LoggerFactory.getLogger(UserComponent.class);
	
	@Autowired
	RestTemplate restTemplate;
	
	@Value("http://localhost:8001/api/user")
	String userURL;

	public UserDto getUser(Long id) throws ApiException {
		try{
			String finalUrl = userURL + '/' + id;
			return  restTemplate.getForEntity(finalUrl, UserDto.class).getBody();
			
		} catch(Exception e) {
			throw new ApiException(UserDto.class.getSimpleName(), id.toString(),
					ExceptionType.NOT_FOUND);
		}
		
	}
}
