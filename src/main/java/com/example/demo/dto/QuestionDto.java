package com.example.demo.dto;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class QuestionDto {
	
	Long id;
	
	@NotNull
	Long idUser;
	
	@NotNull
	List<FlagDto> flags;
	
	@NotNull
	String comment;
	
	@NotNull
	Boolean resolved;
	
	LocalDateTime createdAt;
	LocalDateTime updatedAt;
}
