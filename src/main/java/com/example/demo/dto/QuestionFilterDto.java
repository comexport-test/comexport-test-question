package com.example.demo.dto;

import lombok.Data;

@Data
public class QuestionFilterDto {
	Integer page;
	Integer size;
	String comment;
	Boolean resolved;
}
