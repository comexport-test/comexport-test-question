package com.example.demo.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class QuestionCreateDto {
	
	@NotNull
	Long idUser;
	
	@NotNull
	List<FlagPersistDto> flags;
	
	@NotNull
	String comment;
}
