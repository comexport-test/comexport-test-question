package com.example.demo.dto;

import lombok.Data;

@Data
public class FlagPersistDto {
	
	Long id;
	String description;
}
