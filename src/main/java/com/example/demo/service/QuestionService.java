package com.example.demo.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.component.UserComponent;
import com.example.demo.dto.FlagPersistDto;
import com.example.demo.dto.QuestionCreateDto;
import com.example.demo.dto.QuestionDto;
import com.example.demo.dto.QuestionFilterDto;
import com.example.demo.dto.QuestionUpdateDto;
import com.example.demo.exception.ApiException;
import com.example.demo.exception.ApiExceptionDomain.ExceptionType;
import com.example.demo.model.Flag;
import com.example.demo.model.Question;
import com.example.demo.repository.FlagRepository;
import com.example.demo.repository.QuestionRepository;

@Service
public class QuestionService {

	@Autowired
	private QuestionRepository questionRepository;
	
	@Autowired
	private UserComponent userComponent;
	
	@Autowired
	private FlagRepository flagRepository;

	private ModelMapper modelMapper;

	@Autowired
	public QuestionService(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

	public List<QuestionDto> find(QuestionFilterDto filter) {
		Question questionExample = convertToEntity(filter);

		List<QuestionDto> result = new ArrayList<QuestionDto>();

		int page = filter.getPage() != null ? filter.getPage() : 0;
		int size = filter.getSize() != null && filter.getSize() < 1000 ? filter.getSize() : 1000;
		Pageable basePage = PageRequest.of(page, size);

		ExampleMatcher customExampleMatcher = ExampleMatcher.matchingAll()
				.withMatcher("comment", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())
				.withMatcher("resolved", ExampleMatcher.GenericPropertyMatchers.exact());

		Example<Question> example = Example.of(questionExample, customExampleMatcher);
		Page<Question> queryResult = questionRepository.findAll(example, basePage); 
		queryResult.map(question -> result.add(convertToDto(question)));
		
		return result;
	}
	
	public Optional<QuestionDto> find(Long id) {
		return questionRepository.findById(id).map(question -> convertToDto(question));
	}

	public Long create(@Valid QuestionCreateDto dto) throws ApiException {
		
		userComponent.getUser(dto.getIdUser());
		
		Question question = convertToEntity(dto);
		question.setFlags(validateFlags(dto.getFlags()));

		LocalDateTime now = LocalDateTime.now();
		question.setUpdatedAt(now);
		question.setCreatedAt(now);
		question.setId(null);
		
		questionRepository.save(question);
		return question.getId();
	}

	public void update(@Valid QuestionUpdateDto dto, Long id) throws ApiException {
		
		userComponent.getUser(dto.getIdUser());
		Question original = questionRepository.findById(id)
				.orElseThrow(() -> new ApiException(Question.class.getSimpleName(), 
						id.toString(), ExceptionType.NOT_FOUND));
		
		original.setFlags(validateFlags(dto.getFlags()));
		original.setIdUser(dto.getIdUser());
		
		original.setComment(dto.getComment());
		original.setUpdatedAt(LocalDateTime.now());

		questionRepository.save(original);
	}
	
	public void remove(@Valid Long id) throws ApiException {
		
		//TODO delete answers
		
		Question original = questionRepository.findById(id)
				.orElseThrow(() -> new ApiException(Question.class.getSimpleName(), id.toString(), ExceptionType.NOT_FOUND));
		
		questionRepository.delete(original);
	}
	
	private List<Flag> validateFlags(List<FlagPersistDto> flagDtos) throws ApiException {
		if(flagDtos == null || flagDtos.size() < 1) {
			throw new ApiException(Flag.class.getSimpleName(), " at least one ", 
					ExceptionType.MINIMUM_COUNT);
		}
		
		List<Flag> flags = new ArrayList<Flag>();
		for(FlagPersistDto dto : flagDtos ) {
			flags.add(validateFlag(dto));
		}
		return flags;
	}

	private Flag validateFlag(FlagPersistDto dto) throws ApiException {
		if (dto.getId() != null) {
			return findFlag(dto);
		}

		if (dto.getDescription() != null) {
			Optional<Flag> result = flagRepository.findByDescription(dto.getDescription().toUpperCase());
			if(result.isPresent()) {
				return result.get();
			}
			return buildNewFlag(dto.getDescription().toUpperCase());
		}
		
		throw new ApiException(Flag.class.getSimpleName(), dto.getId().toString(),
				ExceptionType.VALIDATION);
	}

	private Flag buildNewFlag(String description) {
		Flag flag = new Flag();
		flag.setDescription(description);
		flag.setCreatedAt(LocalDateTime.now());
		flag.setUpdatedAt(LocalDateTime.now());
		flag.setEnabled(true);
		flagRepository.save(flag);
		return flag;
	}

	private Flag findFlag(FlagPersistDto dto) throws ApiException {
		Flag flag = flagRepository.findById(dto.getId())
				.orElseThrow(() -> new ApiException(Flag.class.getSimpleName(),
						dto.getId().toString(), ExceptionType.NOT_FOUND));

		if (!flag.isEnabled()) {
			throw new ApiException(Flag.class.getSimpleName(), dto.getId().toString(),
					ExceptionType.NOT_ACTIVE);
		}
		return flag;
	}

	private QuestionDto convertToDto(Question question) {
		QuestionDto dto = modelMapper.map(question, QuestionDto.class);
		return dto;
	}

	private Question convertToEntity(QuestionCreateDto questionDto) {
		Question result = modelMapper.map(questionDto, Question.class);
		return result;
	}
	
	private Question convertToEntity(QuestionFilterDto filter) {
		Question result = modelMapper.map(filter, Question.class);
		return result;
	}
	
	

}
