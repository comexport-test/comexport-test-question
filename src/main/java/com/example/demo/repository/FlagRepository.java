package com.example.demo.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Flag;

public interface FlagRepository extends JpaRepository<Flag, Long> {

	Optional<Flag> findByDescription(String description);

}
