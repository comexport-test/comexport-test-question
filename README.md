### Modificações no modelo original
- Dado escopo e tempo apenas o modulo comexport-test-user possui unit tests.
- Algumas propriedades deveriam estar externalizadas, como as configurações de porta e urls de integração.

### Instalação e Execução
- Prerequisitos : Java, Maven, git, base de dados mysql com schema votes

1) Clonar o projeto para o seu repositório local git
2) Na raiz do projeto, executar 'mvn clean install package'
3) Navegar até o diretório onde o jar foi criado (normalmente /target)
4) Executar java -jar comexport-test-question-1.0.0.jar
5) Swagger se encontra em http://localhost:8002/swagger-ui.html

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.